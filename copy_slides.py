# Joshua Wilkes
# Cyber 1
# Week 4

# Copy Class Slides

# Create a script called copy_slides.py with a function called pptx_copy
# Students will create a script that does the following:
# Finds files in ~/Downloads with the file extension .pptx or .ppt
# Copies these files into the current working directory
# Note: This is another practical script you can use to move downloaded slides from class into your class notes directories.

import os
import shutil

def pptx_copy():
    
    good_path = "c:\\Users\\jcwil\\Downloads"
    print("\nSearching for .pptx and .ppt files...\n")

# use os.listdir to generate the contents from the directory Downloads, and iterate through them

    for file in os.listdir(good_path):
        file_path = os.path.join(good_path, file)

# Searching for ".ppt" will return both file types, and copy them to the CWD

        if file.find(".ppt") > -1:
            print(f"copy {file} to cwd")
            shutil.copy(file_path, "./")

# call the function                

pptx_copy()