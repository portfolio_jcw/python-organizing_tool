# Joshua Wilkes
# Cyber 1
# Weeek 4

# Copying Student Exercises

# So far you've used a few different Python modules, but for the rest of the homework, you will need to familiarize yourself
# with a new one. The shutil module is a Python module used for high-level file operations like moving and copying. Read this
# beforehand to get familiar with shutil and make sure to use the documentation while you're working through the homework.
# Create a script called copy_activities.py with a function called stu_activities that does the following:
# Finds files in ~/Downloads that contain the string Stu_
# Copies these files into the current working directory
# Note: This isn't just a challenge to complete for the sake of it, this is a practical script you can run
# to move any downloaded files from class into your class notes directories.

import os
import shutil

def stu_activities():
    
    good_path = "c:\\Users\\jcwil\\Downloads"
    print("\nSearching for student activities...\n")

# use os.listdir to generate the contents from the directory Downloads, and iterate through them

    for file in os.listdir(good_path):
        file_path = os.path.join(good_path, file)

# use a conditional statement to verify that file is a file and begins with Stu_, then copy it

        if file.startswith("Stu_") and os.path.isfile(file_path):
            print(f"copy {file} to cwd")
            shutil.copy(file_path, "./")
                



            




# Call the function

stu_activities()