# Joshua Wilkes
# Cyber 1
# Week 4

# Class Notes Folder

# Create a script called create_notes_drs.py. In the file, define and call a function called main that does the following:
# Creates a directory called CyberSecurity-Notes in the current working directory
# Within the CyberSecurity-Notes directory, creates 24 sub-directories (sub-folders), called Week 1, Week 2, Week 3,
# and so on until up through Week 24
# Within each week directory, create 3 sub-directories, called Day 1, Day 2, and Day 3
# Bonus Challenge: Add a conditional statement to abort the script if the directory CyberSecurity-Notes already exists.

import os

def main():

# check and see if the directory is already there, then return the build status without building it

    makestatus = "\nDirectory build successful.\n"

    if os.path.isdir("CyberSecurity-Notes"):
        makestatus = "\nDirectory exists.  Build aborted.\n"
            
        return makestatus

# If the conditional is False, exit the conditional and build the directory

    os.makedirs("CyberSecurity-Notes")
    main_path = "CyberSecurity-Notes"

# build the week and day subdirectories

    for w in range(1, 25):
        dpath = os.path.join(main_path, ("Week_" + str(w)))
        os.makedirs(dpath)

        for d in range(1, 4):
            subdpath = os.path.join(dpath, ("Day_" +str(d)))
            os.makedirs(subdpath)

    return makestatus

# call the function

print(main())